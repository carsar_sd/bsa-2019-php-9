<?php

namespace App\Notifications;

use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Support\Facades\Storage;

class ImageProcessedNotification extends Notification
{
    use Queueable;

    /**
     * @var Photo
     */
    public $photo;

    /**
     * ImageProcessedNotification constructor.
     *
     * @param Photo $photo
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return
            (new MailMessage)
                ->subject('Notification Success')
                ->line($notifiable->name.',')
                ->line('You can find your cropped photos below:')
                ->line('
                    <a href="' . Storage::url('public/images/' . $this->photo->user_id . '/cropped/' . $this->photo->photo_100_100) . '" target="_blank">100x100</a>&nbsp;
                    <a href="' . Storage::url('public/images/' . $this->photo->user_id . '/cropped/' . $this->photo->photo_150_150) . '" target="_blank">150x150</a>&nbsp;
                    <a href="' . Storage::url('public/images/' . $this->photo->user_id . '/cropped/' . $this->photo->photo_250_250) . '" target="_blank">250x250</a>
                ')
                ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'status' => $this->photo->status,
            'photo_100_100' => $this->photo->photo_100_100,
            'photo_150_150' => $this->photo->photo_150_150,
            'photo_250_250' => $this->photo->photo_250_250,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'status' => $this->photo->status,
                'photo_100_100' => $this->photo->photo_100_100,
                'photo_150_150' => $this->photo->photo_150_150,
                'photo_250_250' => $this->photo->photo_250_250,
            ]
        );
    }
}
