<?php

namespace App\Notifications;

use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ImageProcessingFailedNotification extends Notification
{
    use Queueable;

    /**
     * @var Photo
     */
    public $photo;

    /**
     * ImageProcessedNotification constructor.
     *
     * @param Photo $photo
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Notification Fail')
            ->line($notifiable->name.',')
            ->line('Something went wrong!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'user' => $notifiable->name,
                'status' => mb_strtolower($this->photo->status),
                'photo_100_100' => $this->photo->photo_100_100,
                'photo_150_150' => $this->photo->photo_150_150,
                'photo_250_250' => $this->photo->photo_250_250,
            ]
        );
    }
}
