<?php

namespace App\Services\Contracts;

use App\Entities\Photo;
use Illuminate\Database\Eloquent\Collection;

interface PhotoService
{
    public function crop(string $pathToPhoto, int $width, int $height): string;

    public function getList(): ?Collection;

    public function getById(int $id): ?Photo;

    public function getByUserId(int $userId): ?Collection;

    public function store(): Photo;

    public function update(Photo $photo): Photo;

    public function delete(Photo $photo): void;
}
