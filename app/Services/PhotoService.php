<?php

namespace App\Services;

use App\Entities\Photo;
use App\Repositories\PhotoRepository;
use App\Services\Contracts\PhotoService as IPhotoService;
use Intervention\Image\Facades\Image;
use Illuminate\Contracts\Filesystem\Factory as Filesystem;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Collection;

class PhotoService implements IPhotoService
{
    private $authService;
    private $fileSystem;
    private $photoRepository;

    public function __construct(Filesystem $fileSystem, AuthService $authService, PhotoRepository $photoRepository)
    {
        $this->fileSystem = $fileSystem;
        $this->authService = $authService;
        $this->photoRepository = $photoRepository;
    }

    public function crop(string $pathToPhoto, int $width, int $height): string
    {
        $croppedImage = Image::make($pathToPhoto)->crop($width, $height);
        $filePath = $croppedImage->filename . '.' . $croppedImage->extension;
        $this->fileSystem->put($filePath, $croppedImage->stream());

        return $filePath;
    }

    public function getList(): ?Collection
    {
        return $this->photoRepository->findAll();
    }

    public function getById(int $id): ?Photo
    {
        return $this->photoRepository->findById($id);
    }

    public function getByUserId(int $userId): ?Collection
    {
        return $this->photoRepository->findByUserId($userId);
    }

    public function store(): Photo
    {
        $user     = $this->authService->getUser();
        $file     = Input::file('photo');
        $fileName = $file->getClientOriginalName();
        $filePath = storage_path('app/public/images/' . $user->id . '/');

        $photo = new Photo([
            'user_id'        => $user->id,
            'original_photo' => $fileName,
            'status'         => Photo::STATUS_PROCESSING,
        ]);

        $file->move($filePath, $fileName);

        return $this->photoRepository->store($photo);
    }

    public function update(Photo $photo): Photo
    {
        $user     = $this->authService->getUser();
        $file     = Input::file('photo');
        $fileName = $file->getClientOriginalName();
        $filePath = storage_path('app/public/images/' . $user->id . '/');

        Storage::delete($filePath);

        $photo->original_photo = $fileName;
        $photo->status         = Photo::STATUS_UPLOADED;

        $file->move($filePath, $fileName);

        return $this->photoRepository->update($photo);
    }

    public function delete(Photo $photo): void
    {
        $user     = $this->authService->getUser();
        $filePath = storage_path('app/public/images/' . $user->id . '/');
        Storage::delete($filePath);
        $this->photoRepository->delete($photo);
    }
}
