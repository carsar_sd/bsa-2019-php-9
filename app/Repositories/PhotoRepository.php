<?php

namespace App\Repositories;

use App\Jobs\CropJob;
use App\Repositories\Contracts\PhotoRepository as IPhotoRepository;
use App\Entities\Photo;
use Illuminate\Database\Eloquent\Collection;

class PhotoRepository implements IPhotoRepository
{
    public function findAll(): ?Collection
    {
        return Photo::all();
    }

    public function findById(int $id): ?Photo
    {
        return Photo::where('id', $id)->first();
    }

    public function findByUserId(int $user_id): ?Collection
    {
        return Photo::where('user_id', $user_id)->first();
    }

    public function store(Photo $photo): Photo
    {
        $photo->save();

        CropJob::dispatch($photo);

        return $photo;
    }

    public function update(Photo $photo): Photo
    {
        $photo->save();

        CropJob::dispatch($photo);

        return $photo;
    }

    public function delete(Photo $photo): void
    {
        $photo->delete();
    }
}
