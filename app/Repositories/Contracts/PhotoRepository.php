<?php

namespace App\Repositories\Contracts;

use App\Entities\Photo;
use Illuminate\Database\Eloquent\Collection;

interface PhotoRepository
{
    public function findAll(): ?Collection;

    public function findById(int $id): ?Photo;

    public function findByUserId(int $userId): ?Collection;

    public function store(Photo $photo): Photo;

    public function update(Photo $photo): Photo;

    public function delete(Photo $photo): void;
}
