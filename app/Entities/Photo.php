<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Photo
 *
 * @package App\Entity
 * @property int $id
 * @property int $user_id
 * @property string $original_photo
 * @property string $photo_100_100
 * @property string $photo_150_150
 * @property string $photo_250_250
 * @property string $status
 * @property User $user
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Photo extends Model
{
    const STATUS_UPLOADED   = 'UPLOADED';
    const STATUS_PROCESSING = 'PROCESSING';
    const STATUS_SUCCESS    = 'SUCCESS';
    const STATUS_FAIL       = 'FAIL';

    protected $table = 'photos';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'original_photo', 'photo_100_100', 'photo_150_150', 'photo_250_250'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
