<?php

namespace App\Jobs;

use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Services\Contracts\PhotoService;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $photo;

    /**
     * CropJob constructor.
     *
     * @param Photo $photo
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * @param PhotoService $photoService
     */
    public function handle(PhotoService $photoService)
    {
        $this->photo->status = Photo::STATUS_PROCESSING;
        $photoName  = $this->photo->original_photo;
        $parentDir  = storage_path('app/public/images/' . $this->photo->user_id . '/');
        $croppedDir = '/images/' . $this->photo->user_id . '/cropped/';

        foreach ([100, 150, 250] as $item) {
            $cropped  = $photoService->crop($parentDir . $photoName, $item, $item);
            $fileData = pathinfo($photoName);
            $cropped_name  = $fileData['filename'] . $item . 'x' . $item . '.' . $fileData['extension'];
            $cropped_field = "photo_{$item}_{$item}";
            $this->photo->$cropped_field = $cropped_name;
            Storage::move($cropped, $croppedDir . $cropped_name);
        }

        $this->photo->status = Photo::STATUS_SUCCESS;
        $this->photo->save();

        $this->photo->user->notify(new ImageProcessedNotification($this->photo));
    }

    /**
     * Fail the job.
     *
     * @return void
     */
    public function failed()
    {
        $this->photo->status = Photo::STATUS_FAIL;
        $this->photo->save();
        $this->photo->user->notify(new ImageProcessingFailedNotification($this->photo));
    }
}
