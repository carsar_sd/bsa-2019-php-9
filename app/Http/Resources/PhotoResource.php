<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PhotoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'original'   => $this->original_photo,
            'resize_100' => $this->photo_100_100,
            'resize_150' => $this->photo_150_150,
            'resize_250' => $this->photo_250_250,
            'status'     => $this->status,
            'user_id'    => $this->user->id,
            'user_name'  => $this->user->name,
        ];
    }
}
