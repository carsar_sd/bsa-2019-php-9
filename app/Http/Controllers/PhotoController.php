<?php

namespace App\Http\Controllers;

use Auth;
use App\Services\PhotoService;
use App\Http\Resources\PhotoResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class PhotoController extends Controller
{
    private $photoService;

    public function __construct(PhotoService $photoService)
    {
        $this->photoService = $photoService;
        /*echo "<pre>"; print_r(
            base64_encode('{"email": "wisozk.leonie@example.com","password": "password"}')
        ); echo "</pre>"; exit();*/
        //Bearer eyJlbWFpbCI6ICJ3aXNvemsubGVvbmllQGV4YW1wbGUuY29tIiwicGFzc3dvcmQiOiAicGFzc3dvcmQifQ==
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = $this->photoService->getList();

        if (!$photos) {
            return new Response(
                ['status' => Response::$statusTexts[Response::HTTP_NOT_FOUND]], Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            $photos->map(function ($photo) {
                return new PhotoResource($photo);
            }), Response::HTTP_OK
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $photo = $this->photoService->store();

            return new Response(
                new PhotoResource($photo), Response::HTTP_OK
            );
        } catch (ValidationException $e) {
            return new Response(
                ['error' => $e->getMessage()], $e->getCode()
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $photo = $this->photoService->getById($id);

        if (!$photo) {
            return new Response(
                ['status' => Response::$statusTexts[Response::HTTP_NOT_FOUND]], Response::HTTP_NOT_FOUND
            );
        }

        return new Response(
            new PhotoResource($photo), Response::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        try {
            $this->validate($request, [
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $photo = $this->photoService->getById($id);

            if (!$photo) {
                return new Response(
                    ['status' => Response::$statusTexts[Response::HTTP_NOT_FOUND]], Response::HTTP_NOT_FOUND
                );
            }

            $photo = $this->photoService->update($photo);

            return new Response(
                new PhotoResource($photo), Response::HTTP_OK
            );
        } catch (ValidationException $e) {
            return new Response(
                ['status' => $e->getMessage()], $e->getCode()
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $photo = $this->photoService->getById($id);

        if (!$photo) {
            return new Response(
                ['status' => Response::$statusTexts[Response::HTTP_NOT_FOUND]], Response::HTTP_NOT_FOUND
            );
        } elseif ($photo->user_id !== Auth::id()) {
            return new Response(
                ['status' => Response::$statusTexts[Response::HTTP_FORBIDDEN]], Response::HTTP_FORBIDDEN
            );
        }

        $this->photoService->delete($photo);

        return new Response(
            ['status' => 'success'], Response::HTTP_NO_CONTENT
        );
    }
}
